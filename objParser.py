def parse(path):
    finalPolygons = ""
    objFile = open(path, "r")
    objDatas = objFile.readlines()
    vertices = []
    normals = []
    textures = []
    objectData = []
    objectIndexDelimiter = 0
    index = 0

    flatten = lambda t: [item for sublist in t for item in sublist]

    while (index < len(objDatas)):
        faces = []

        if objDatas[index].replace("\n","").split(" ")[0].lower() != 'o' :
            index += 1
            continue

        # Register Object Data
        objectData = objDatas[index].replace("\n","").split(" ")

        for i in range(index + 1,len(objDatas)):
            # Recieve and categorize data by its correct format
            clean = objDatas[i].replace("\n","").split(" ")
            if clean[0].lower() == 'v' :
                vertices.append(clean[1:])
            elif clean[0].lower() == 'vt' : 
                textures.append(clean[1:])
            elif clean[0].lower() == 'vn' : 
                normals.append(clean[1:])
            elif clean[0].lower() == 'f' : 
                faces.append(clean[1:])
            elif clean[0].lower() == 'o' :
                index = i - 1
                break
        if len(faces) > 0 :       
            # Parse Data into OpenGL Readabale
            try:

                def getVertices(indexes):
                    v = vertices[int(indexes[0]) - 1]
                    t = textures[int(indexes[1]) - 1]
                    n = normals[int(indexes[2]) - 1]
                    return strigifyList(v, False) + "," + strigifyList(n, False) + "," + strigifyList(t, False)
                
                def strigifyList(li, isAbsolute):
                    result = ""
                    for item in li:
                        if isAbsolute:
                            result += item.replace("-","") + ","
                            print(result)
                        else:
                            result += item + ","
                    return result[:-1]

                count = 0
                for face in faces:
                    # Triangulation
                    i = 0
                    central_vertex = getVertices(face[0].split("/"))
                    while i + 2 < len(face):
                        vertex_1 = getVertices(face[i+1].split("/"))
                        vertex_2 = getVertices(face[i+2].split("/"))
                        finalPolygons += central_vertex + "\n" + vertex_1 + "\n" + vertex_2 + "\n"
                        count += 3
                        i += 1
                finalPolygons += f'i,{objectIndexDelimiter},{count+1},{objectData[1]}\n'
                objectIndexDelimiter += count
            except Exception as e:
                print("Error : OBJ is either invalid or corrupted")
        index += 1

    open("OpenGLFormatOutput.txt", "w").writelines(finalPolygons)
    print("Parsing Complete!")

if __name__ == '__main__':
    parse("./Bucket_only_better.obj")
    